#include <pthread.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <iostream>
#include <sstream>


// Creacion de la estructura.
 struct file {
	int lineas = 0;
	int palabras = 0;
	int caracteres = 0;	
     };

void *contadores (void *param) {;
	// creacioin de variables.
	char *str, *ultima_linea, c[2];
	int lineas = 0;
	int caracteres = 0;
	int palabras = 0;
	str = (char *) param;
	
	// Ya que nose puede retornar un valor local de estra funcion,
	// Se crea un puntero con malloc para darle un espacio en la memoria al valor que se desea retornar
	struct file* pointer = (struct file *) malloc (sizeof (struct file));
	
	// linea de codigo que abre el archivo que se ponga en la terminal.
	int stream = open (str, O_RDONLY);
	
	// Si devuelve un valor -1 no se pudo abrir el archivo.
	if (stream == -1) {
		std::cout << "Error al abrir archivo" << std::endl;
		pthread_exit(0);
	}
	
	// Mientras que otro valor lee el archivo.
	while (read(stream,c,1)) { 
		// Aumenta contador de caracteres.
		caracteres++;
		// Variable que se usara para controlar la ultima linea del archivo.
		ultima_linea = c;
		// if que cuenta todas las lineas del programa.
		if (strchr("\n", c[0])) {
			lineas++;
		}
		// if que cuenta los espacios del programa.
		if (strchr(" ", c[0])) {
			palabras++;
			
		}
	}
	// Condicion por si el archivo no tiene una ultima, sumara 1 a las lineas
	if (ultima_linea != "\n"){
		lineas++;
	}
	close (stream);
	// Operaciones para obtener las lineas, caracteres y palabras.
	palabras = palabras + lineas;
	caracteres = caracteres - lineas;
	pointer->caracteres = caracteres;
	pointer->palabras = palabras;
	pointer->lineas = lineas;
	std::ostringstream oss;
	oss << lineas;
	std::string t = "En el archivo " + (std::string ) str + " se encontraron " + oss.str() + " lineas, ";
	std::cout << t << palabras  << " palabras y " << caracteres << " caracteres " << std::endl;

	pthread_exit(pointer);
	}

// Funcion para medir el tiempo.
float time_c(float inicialTime, float finalTime){
    float ejecutionTime = (finalTime - inicialTime)/CLOCKS_PER_SEC;
    return ejecutionTime;
}

int main(int argc, char *argv[]) {
	// Creacion de variables
	pthread_t threads[argc - 1];
	file* res[argc - 1];
	int total_caracteres = 0;
	int total_palabras = 0;
	int total_lineas = 0;
	float inicialTime, finalTime;
	
	inicialTime = clock();
	// Crea los hilos.
	for (int i=0; i < argc - 1; i++) {
    pthread_create(&threads[i], NULL, contadores, argv[i+1]);
	}

	// Espera el termino de los hilos.
	for (int i=0; i< argc - 1; i++) {
		pthread_join(threads[i], (void**) &res[i]);
		// Operaciones para contar el total de palabras.
		total_caracteres += res[i]->caracteres;
		total_palabras += res[i]->palabras;
		total_lineas += res[i]->lineas;
	}
	finalTime = clock();
	std::cout << "El total de lineas es " << total_lineas << ", el total de palabras es " << total_palabras << " y el total de caracteres es " << total_caracteres  << std::endl;
	std::cout << "Tiempo de ejecucion es: " << time_c(inicialTime, finalTime) << " segundos" << std::endl;
	return 0;
}
