**Laboratorio 3 Unidad 2**                                                                                                                            
Programa basado en hebras                                                                                                                                                        
# Pre-requisitos                                                                                                                                       
C++                                                                                                                                                 
Ubuntu                                                                                                                                                  
Make
                                                                                                                                                    
# Instalación                                                                                                                                                                                                                                                                                                                                                                                                                                      
-Instalar Make si no esta en su computador.                                
Para instalar Make inicie la terminal y escriba lo siguiente:

sudo apt install make                                                 


# Problematica

Modificar el programa secuencial para que cree un thread por cada archivo a contar y obtener igualmente el total
# Ejecutando

Para abrir el programa necesita ejecutar el archivo make desde la terminal, luego escribir en la terminal: ./Hebras (nombre del archivo). Luego de esto el programa empezara a imprimir la cantidad de lineas, caracteres y palabras junto con el total del archivo u los archivos.

Para comparar el tiempo entre ambos programas este le indica al final el tiempo que se demoro.

**Construido con**                                                                                                                                      
C++
# Librerias:

pthread.h                                                                                                                                               
unistd.h                                                                                                                                                
fcntl.h                                                                                                                                                 
string.h                                                                                                                                                  
iostream                                                                                                                                                
sstream                                                                                                                                                         

**Versionado**                                                                                                                                          
Version 1.2                                                                                                                                             
**Autores**                                                                                                                                                     
Rodrigo Valenzuela
