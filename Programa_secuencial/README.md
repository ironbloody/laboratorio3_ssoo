**Laboratorio 3 Unidad 2**                                                                                                                            
Programa basado en hebras                                                                                                                                                        
# Pre-requisitos                                                                                                                                       
C++                                                                                                                                                 
Ubuntu                                                                                                                                                  
Make
                                                                                                                                                    
# Instalación                                                                                                                                                                                                                                                                                                                                                                                                                                      
-Instalar Make si no esta en su computador.                                
Para instalar Make inicie la terminal y escriba lo siguiente:

sudo apt install make                                                 


# Problematica

Crear un programa que recibe una lista de archivos y a partir de ellos cuente la cantidad de lineas, palabras y caracteres, junto con el total de estas variables entre todos los archivos
# Ejecutando

Para abrir el programa necesita ejecutar el archivo make desde la terminal, luego escribir en la terminal: ./Secuencial (nombre del archivo). Luego de esto el programa empezara a imprimir la cantidad de lineas, caracteres y palabras junto con el total del archivo u los archivos.

Para comparar el tiempo entre ambos programas este le indica al final el tiempo que se demoro.

**Construido con**                                                                                                                                      
C++
# Librerias:
                                                                                                                                              
unistd.h                                                                                                                                                
fcntl.h                                                                                                                                                 
string.h                                                                                                                                                  
iostream                                                                                                                                                
sstream                                                                                                                                                         

**Versionado**                                                                                                                                          
Version 1.2                                                                                                                                             
**Autores**                                                                                                                                                     
Rodrigo Valenzuela
