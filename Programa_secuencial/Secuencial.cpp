#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <iostream>
#include <sstream>

// Funcion que contiene los calculos para las lineas, palabras y caracteres.
void contadores(void *param, int &palabras_totales,int &lineas_totales, int &caracteres_totales){
	// Creacion de variables.
	char *str, *ultima_linea, c[2];
	int lineas = 0;
	int palabras = 0;
	int caracteres = 0;
	str = (char *) param;
	
	// linea de codigo que abre el archivo que se ponga en la terminal.
	int stream = open(str, O_RDONLY);
	
	// Si devuelve un valor -1 no se pudo abrir el archivo.
	if (stream == -1) {
		std::cout << "Error" << std::endl;
		pthread_exit(0);
	}
	// Mientras que otro valor lee el archivo.
	while (read(stream,c,1)) {
		// Aumenta contador de caracteres.
		caracteres++;
		// Variable que se usara para controlar la ultima linea del archivo.
		ultima_linea = c;
		// if que cuenta todas las lineas del programa.
		if (strchr("\n", c[0])) {
			lineas++;
		}
		// if que cuenta los espacios del programa.
		if (strchr(" ", c[0])) {
			palabras++;
			
		}
	}
	// Condicion por si el archivo no tiene una ultima, sumara 1 a las lineas
	if (ultima_linea != "\n"){
		lineas++;
	}
	close (stream);
	// Operaciones para obtener las lineas, caracteres y palabras.
	caracteres = caracteres - lineas;
	palabras = palabras + lineas;
	lineas_totales += lineas;
	palabras_totales += palabras;
	caracteres_totales += caracteres;
	
	std::ostringstream oss;
	oss << lineas;
	std::string t = "En el archivo " + (std::string ) str + " se encontraron " + oss.str() + " lineas, ";
	std::cout << t << palabras  << " palabras y " << caracteres << " caracteres " << std::endl;
	std::cout << "lineas totales: " << lineas_totales << ", Palabras totales: " << palabras_totales << ", caracteres totales: " << caracteres_totales << std::endl;

}
// Funcion para medir el tiempo.
float time_c(float inicialTime, float finalTime){
    float ejecutionTime = (finalTime - inicialTime)/CLOCKS_PER_SEC;
    return ejecutionTime;
}

int main(int argc, char *argv[]) {
	// Creacion de variables.
	int palabras_totales = 0;
	int lineas_totales = 0;
	int caracteres_totales = 0;
	float inicialTime, finalTime;
	inicialTime = clock();
	for (int i=0; i < argc - 1; i++) {
		// Se llama a la funcion argv sera el valor que se le ingresa a la terminal.
		contadores(argv[i+1], palabras_totales, lineas_totales, caracteres_totales);
	}
	finalTime = clock();
	std::cout << "Tiempo de ejecucion es: " << time_c(inicialTime, finalTime) << " segundos" << std::endl;
	
	return 0;
}


